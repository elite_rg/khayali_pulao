class CreateOrders < ActiveRecord::Migration
  def change
    create_table :orders do |t|
      t.string :status, :default => "pending"
      t.integer :total
      t.references :user, index: true, foreign_key: true
      t.integer :vat
      t.string :transaction_id
      t.integer :delivery_cost

      t.timestamps null: false
    end
  end
end
