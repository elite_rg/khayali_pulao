# This file should contain all the record creation needed to seed the database with its default values.
# The data can then be loaded with the rake db:seed (or created alongside the db with db:setup).
#
# Examples:
#
#   cities = City.create([{ name: 'Chicago' }, { name: 'Copenhagen' }])
#   Mayor.create(name: 'Emanuel', city: cities.first)

User.create(email: "ravighare26@gmail.com", password: "password")

Food.create(name: "Veg roll",
                description: "Veg roll",
                price: 50, image: File.open("#{Rails.root}/public/1.jpg"))
Food.create(name: "Vada pav",
            description: "yummy vada pav",
            price: 30, image: File.open("#{Rails.root}/public/2.jpg"))
Food.create(name: "Burger",
            description: "chicken burger",
            price: 100, image: File.open("#{Rails.root}/public/3.jpg"))
Food.create(name: "Veg biryani",
            description: "colorful veg biryani",
            price: 150, image: File.open("#{Rails.root}/public/4.jpg"))