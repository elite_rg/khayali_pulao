# README #

Small rails application capable to take food orders from user.

### How do I get set up? ###
Generate user related database and its table

```
#!ruby

rake db:create
rake db:migrate
```


Generate food item related database and its table

```
#!ruby

rake foods:db:create
rake foods:db:migrate
```

Generate user and food items


```
#!ruby

rake db:seed
```