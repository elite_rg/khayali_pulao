class CartsController < ApplicationController
  def index
    @cart_items = session[:cart]
    @ordered_foods = {}
    @total = 0
    unless session[:cart].nil?
      @cart_items.each do |food_id, qty|
        food = Food.find_by_id(food_id)
        @ordered_foods[food_id] = { food: food, qty: qty }
        @total += (food.price * qty)
      end
    end
    # debugger
    @current_order.ordered_items = @ordered_foods
    session[:order]["details"]["total"] = @total
    session[:order]["items"] = @ordered_foods
  end

  def destroy
    food_id = params[:id]
    @cart.cart_data.delete(food_id)
    redirect_to carts_path
  end

end
