class CartItemsController < ApplicationController

  def create
    @cart.increment(params[:food_id])
    session[:cart] = @cart.cart_data
    redirect_to foods_path
  end

end
