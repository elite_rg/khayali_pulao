class Current_Order
  attr_accessor :ordered_items
  attr_accessor :total
  attr_accessor :delivery_cost
  attr_accessor :final_amount

  def initialize(current_order)
    @ordered_items = current_order["items"] || {}
    current_order["details"] ||= {}
    @total = current_order["details"]["total"].to_i || 0
    @delivery_cost = 500
    @final_amount = current_order["details"]["final_amount"].to_i || 0
    @user = {}
    @transaction_id = ""
    @status = "pending"
  end

  def vat
    0.1 * @total
  end

  def update_order(order, args)
    @ordered_items = order["items"] || {}
    @total = order["details"]["total"] || 0
    @transaction_id = args[:transaction_id] || ""
    @status = args[:status] || "pending"
  end

  def save_order(current_user)
    user = current_user
    new_order = user.orders.new(total: @final_amount, vat: vat,
                                delivery_cost: @delivery_cost,
                                transaction_id: @transaction_id, status: @status)
    save_successful = new_order.save
    if save_successful
      @ordered_items.each do |index, details|
        new_order.order_items <<
          OrderItem.create(food_id: details["food"]["id"],
                           quantity: details["qty"])
      end
    end
    save_successful
  end

end
