class FoodBase < ActiveRecord::Base
  establish_connection DB_FOODS
  self.abstract_class = true
end